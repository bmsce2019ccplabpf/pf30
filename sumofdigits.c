#include<stdio.h>
int input()
{
    int num;
    printf("Enter integer value: \n");
    scanf("%d",&num);
    return num;
}
int compute(int num)
{
    int temp,sum=0;
    while(num!=0)
    {
        temp=num%10;
        sum=sum+temp;
        num=num/10;
    }
    return sum;
}
void output(int sum)
{
    printf("The sum of digits = %d",sum);
}
void main()
{
    int num,sum;
    num = input();
    sum = compute(num);
    output(sum);
}